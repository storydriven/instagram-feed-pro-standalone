
===================== Instagram Feed Standalone ======================

Author: Smash Balloon
Support Website: http://smashballoon.com/instagram-feed/support
Version: 3.3
License: Non-distributable, not for resale


========== How to use the Instagram Feed Standalone Version ==========

Open the 'example.html' file for step-by-step instructions on how to
set up and configure your feed.

To display multiple feeds, just follow the directions in the
'multiple.html' file.

For more detailed instructions please visit the documentation section
of the Smash Balloon website:
http://smashballoon.com/instagram-feed/standalone/docs